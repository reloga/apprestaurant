package com.example.appcocina.api;

public class ApiConstants {
    static final String BASE_URL = "https://diplomado-restaurant-backend.herokuapp.com/";
    static final int TIMEOUT = 60; // sec

    static final String LOGIN = "auth/usuarios-login";
}
