package com.example.appcocina.api;

import com.example.appcocina.api.request.LoginRequest;
import com.example.appcocina.api.response.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiService {
    @POST(ApiConstants.LOGIN)
    Call<LoginResponse> login(@Body LoginRequest request);
}
