package com.example.appcocina.api.response;

public class LoginResponse {
    private String message;
    private User data;

    public String getMessage() {
        return message;
    }

    public User getData() {
        return data;
    }
}
